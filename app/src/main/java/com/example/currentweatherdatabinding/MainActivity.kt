package com.example.currentweatherdatabinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.databinding.DataBindingUtil
import com.example.currentweatherdatabinding.databinding.ActivityMainBinding
import com.google.gson.Gson
import com.google.gson.JsonParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.InputStream
import java.io.InputStreamReader
import java.net.URL
import java.util.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var gson: Gson
    private lateinit var citiesRaw : Array<City>
    private lateinit var city: City

    lateinit var binding: ActivityMainBinding
    lateinit var temp: CityTemperature
    lateinit var directionView: ImageView
    lateinit var cloudnessView: ImageView
    private var API_KEY = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding  = DataBindingUtil.setContentView(this, R.layout.activity_main)
        this.temp = CityTemperature(name="Choose city")
        binding.city = this.temp
        API_KEY = getString(R.string.API_KEY)


        val spinner = findViewById<Spinner>(R.id.spinner)
        directionView = findViewById<ImageView>(R.id.direction)
        cloudnessView = findViewById<ImageView>(R.id.cloudness)

        //working with json
        gson = Gson()
        val cities_stream = resources.openRawResource(R.raw.russian_cities)
        citiesRaw = gson.fromJson(InputStreamReader(cities_stream), Cities::class.java).cities
        var city_names: ArrayList<String> = ArrayList()
        for (city in citiesRaw) {
            city_names.add(city.name)
        }

        val adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, city_names)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this

    }
    suspend fun loadWeather() {
        Log.d("mytag", "query start")
        try {
            val weatherURL = "https://api.openweathermap.org/data/2.5/weather?lat=${city.coords.lat}&lon=${city.coords.lon}&appid=$API_KEY&units=metric"
            val stream = URL(weatherURL).getContent() as InputStream
            val data = Scanner(stream).nextLine()
            Log.d("mytag", "java class name:" +  data::class)
            Log.d("mytag", data)

            val parser = JsonParser().parse(data).asJsonObject
            this.temp.temperature = parser.get("main").asJsonObject.get("temp").toString().toFloat()
            this.temp.windSpeed = parser.get("wind").asJsonObject.get("speed").toString().toFloat()
            this.temp.windDirection = parser.get("wind").asJsonObject.get("deg").toString().toFloat()
            this.temp.clouds = parser.get("clouds").asJsonObject.get("all").toString().toInt()
            Log.d("mytag", "Cloudness:" + this.temp.clouds.toString())
            this.binding.city = this.temp

            // setting the wind direction
            if (135 > this.temp.windDirection && this.temp.windDirection >= 45) {
                directionView.setImageResource(R.drawable.east)
            } else if (225 > this.temp.windDirection && this.temp.windDirection >= 135) {
                directionView.setImageResource(R.drawable.south)
            } else if (315 > this.temp.windDirection && this.temp.windDirection >= 225) {
                directionView.setImageResource(R.drawable.west)
            } else {
                directionView.setImageResource(R.drawable.north)
            }

            when {
                temp.clouds <= 33 -> {
                    cloudnessView.setImageResource(R.drawable.sunny)
                }
                temp.clouds >= 70 -> {
                    cloudnessView.setImageResource(R.drawable.partly_cloudy)
                }
                else -> {
                    cloudnessView.setImageResource(R.drawable.cloud)
                }
            }
        } catch (e: Exception) {
            this.temp.name = "Try again!"
            this.binding.city = this.temp

        }
    }


    public fun onClick(v: View) {
        // Используем IO-диспетчер вместо Main (основного потока)
        GlobalScope.launch (Dispatchers.IO) {
            loadWeather()
        }
    }


    // spinner listener
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val cityIndex = position
        city = citiesRaw[cityIndex]
        this.temp.name = city.name
        Log.d("mytag", city.toString())
    }


    // spinner listener
    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

}