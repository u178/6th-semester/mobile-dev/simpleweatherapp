package com.example.currentweatherdatabinding

import com.google.gson.annotations.SerializedName

data class City (
    @SerializedName("coords") val coords: Coord,
    @SerializedName("district") val district: String,
    @SerializedName("name") val name: String,
    @SerializedName("population") val population: String,
    @SerializedName("subject") val subject: String
    ){data class Coord (
            @SerializedName("lat") val lat: Float,
            @SerializedName("lon") val lon: Float
    )
}