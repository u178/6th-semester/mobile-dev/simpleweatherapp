all cities were taken from: https://github.com/pensnarik/russian-cities

You should wrap the original content of "russian-cities.json" into the new root element cities.

Also rename the file to "russian_cities.json" and move to the app/res/raw folder

```json
{
  "cities":
  [
    "original content"
  ]
}
```

### Examples

![city1.png](./raw/city1.png)

![starting_screen.png](./raw/starting_screen.png)

![city2.png](./raw/city2.png)

![city3.png](./raw/city3.png)

![city4.png](./raw/city4.png)
